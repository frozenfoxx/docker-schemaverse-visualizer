# Base image
FROM php:7.2-apache

# Information
LABEL maintainer="FrozenFOXX <frozenfoxx@churchoffoxx.net>"

# Variables
ENV DATABASE="schemaverse" \
  PORT="5432" \
  HOST="172.17.0.1" \
  USERNAME="schemaverse" \
  PASSWORD="schemaverse"

# Install packages
RUN apt-get update \
    && apt-get install -y \
      git \
      libpq-dev \
    && docker-php-ext-install \
      pgsql \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Clone down Schemaverse-Visualizer
WORKDIR /src
RUN git clone https://github.com/marcneuwirth/schemaverse-visualizer.git schemaverse-visualizer

# Configure
RUN cp -rp /src/schemaverse-visualizer/www/* /var/www/html/
COPY conf/setup.php /var/www/html/

# Expose ports
EXPOSE 80
