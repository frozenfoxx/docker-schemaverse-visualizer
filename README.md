# docker-schemaverse-visualizer
A [Docker](https://www.docker.com) container running the [schemaverse-visualizer](https://github.com/marcneuwirth/schemaverse-visualizer).

Docker Hub: [https://hub.docker.com/r/frozenfoxx/schemaverse-visualizer/](https://hub.docker.com/r/frozenfoxx/schemaverse-visualizer/)

# How to Build
```
git clone git@gitlab.com:frozenfoxx/docker-schemaverse-visualizer.git
cd docker-schemaverse-visualizer
docker build .
```

# How to Use this Image
## Quickstart
The following will set up and run the latest schemaverse-visualizer:

```
docker run --rm --name=schemaverse-visualizer -p 80:80 frozenfoxx/schemaverse-visualizer
```

## Usage
The standard deployment will usually involve setting the environment variables for tuning the configuration:

```
docker run -d --rm --name=schemaverse_visualizer \
  -p 80:80 \
  -e PORT="5432" \
  -e USER="someuser" \
  -e PASSWORD="somepassword" \
  frozenfoxx/schemaverse-visualizer
```

## Docker Compose
You can also adjust and deploy the server via the `docker-compose.yml` as such:

```
docker-compose up -d schemaverse-visualizer
```

# Configuration
## Environment Variables
This image can be configured using environment variables. Set this during startup to reconfigure.

* `DATABASE`: name of the database to connect to (default: `schemaverse`).
* `PORT`: port the database is listening on (default: `5432`).
* `HOST`: IP or FQDN of the host running Schemaverse (default: `172.17.0.1`).
* `USERNAME`: user to connect to the database with (default: `schemaverse`).
* `PASSWORD`: password of the user to connect to the database with (default: `schemaverse`).
