<?php

function connect(){
    $database = getenv('DATABASE');
    $host     = getenv('HOST');
    $username = getenv('USERNAME');
    $password = getenv('PASSWORD');
    $port     = getenv('PORT');
    return $conn=pg_connect("host=$host port=$port user=$username dbname=$database password=$password connect_timeout=60");
}

?>